# What's LambdaChip RFC

The LambdaChip RFC (request for comments) process is the way to add new features and standard API to LambdaChip system.

# The workflow

- Open an issue for discussion, and post to [LambdaChip subreddit](https://www.reddit.com/r/lambdachip/) with **RFC flair** to attract people to discuss.
- If it's considered a valid request, the maintainer will allocate a RFC number and prepare a blank markdown file (RFC file).
- Keep discussion in the issue, and send Merge Request to the RFC file.
- When the RFC is finished, maintainer will close the issue.
- Start to implement the RFC.

# What is a valid request?

- Not violate the basic design of LambdaChip.
- Should be achievable.
- Not requested before.
- Respect other stakeholders' benefit.
- Keep opening mind, free-and-opensource.

## Backward compatibility

There're two rules:

- Rule 1: Don't break the compatibility.
- Rule 2: If we have to break it for reasons, add it to major release, say, 0.0.3 to 1.0.0. In major release, the design or architecture is not guaranteed to be
backward compatible. **However, the major release will be a slow and long term process.**
